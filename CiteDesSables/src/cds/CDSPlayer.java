package cds;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import clan.Clan;
import clan.ClanInvite;
import habitations.Habitation;

public class CDSPlayer implements Serializable, Comparable<CDSPlayer> {
	
	private static final long serialVersionUID = -8238845041096286358L;
	private String name;
	private List<Habitation> habitations;
	private transient ClanInvite clanInvite;
	private Clan clan;
	private int minedEmeralds;
	private int bank;
	
	
	public CDSPlayer(String name) {
		this.name = name;
		this.minedEmeralds = 0;
		this.bank = 0;
		this.habitations = new ArrayList<Habitation>();
	}
	
	public String getName() {
        return name;
    }
	
	public Player getBukkitPlayer() {
        return Bukkit.getPlayer(this.name);
    }
	
	public void addEmerald() {
		this.minedEmeralds++;
	}
	
	public int getNumberOfEmeralds() {
		return this.minedEmeralds;
	}
	
	public int getTickPlayTime() {
		return this.getBukkitPlayer().getTicksLived();
	}
	
	public void setClan(Clan clan) {
		this.clan = clan;
	}
	
	public void addFoundsToBank(int amount) {
		this.bank += amount;
	}
	
	public void removeFoundsFromBank(int amount) {
		if (!(amount - this.bank < 0)) {
			this.bank -= amount;
		}
		
		else {
			Bukkit.getServer().getPlayer(this.name).sendMessage(CiteDesSables.prefix + "Vous n'avez pas assez d'argent sur votre compte en banque");
		}
	}
	
	public int getBankAmount() {
		return this.bank;
	}
	
	public ClanInvite getClanInvite() {
		return this.clanInvite;
	}
	
	public void setClanInvite(ClanInvite ci) {
		this.clanInvite = ci;
	}
	
	public void acceptClanInvite() {
		this.setClan(this.getClanInvite().getClan());
		CiteDesSables.getInstance().getClanManager().getClanByName(this.getClanInvite().getClan().getName()).addMember(this);
		for (CDSPlayer p : this.getClanInvite().getClan().getPlayers()) {
			Bukkit.getServer().getPlayer(p.getName()).sendMessage(CiteDesSables.prefix + "Le joueur §a" + this.getName() + "§7 a rejoint le clan");
		}
		this.setClanInvite(null);
	}
	
	public boolean hasClan() {
		return this.clan != null;
	}
	
	public Clan getClan() {
		return this.clan;
	}
	
	public boolean buyHabitation(Habitation h) {
		boolean bought = false;
		int price = h.getPrice();
		if (h.getOwner() != null) {
			if (!h.getOwner().equalsIgnoreCase(this.name)) {
				this.getBukkitPlayer().sendMessage(CiteDesSables.prefix + "§4Cette habitation est déjà occupée");
			}
			else {
				this.getBukkitPlayer().sendMessage(CiteDesSables.prefix + "§cVous avez déjà acheté cette habitation");
			}
		}
		else if (price > this.getBankAmount()) {
			this.getBukkitPlayer().sendMessage(CiteDesSables.prefix + "§cVous n'avez pas assez d'émeraudes pour acheter cette habitation");
		}
		else {
			this.getBukkitPlayer().sendMessage(CiteDesSables.prefix + "§aVous avez acheté l'habitation " + h.getRegion().getName() + " pour le prix de " +
		 price);
			this.minedEmeralds -= price;
			h.setOwner(this.name);
			this.habitations.add(h);
			bought = true;
			this.writeToFS();
		}
		return bought;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CDSPlayer other = (CDSPlayer) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	public void loadConfig() {
		File f = new File("plugins/CiteDesSables/players/" + this.name + ".data");
		if (f.exists()) {
			CDSPlayer player = this.readFromFS();
			this.minedEmeralds = player.getNumberOfEmeralds();
			this.bank = player.getBankAmount();
			this.clan = player.getClan();
			//habitations
		}
	}
	
	public boolean exists() {
		File f = new File("plugins/CiteDesSables/players/" + this.name + ".data");
		return f.exists();
	}
	
	public void writeToFS() {
		File dir = new File("plugins/CiteDesSables/players/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/players/" + this.name + ".data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
            System.out.println("PLAYERS : " + this.name + " has been saved");
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : " + this.name + ".data");
			e.printStackTrace();
		}
	}
	
	public CDSPlayer readFromFS() {
		CDSPlayer ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/players/" + this.name + ".data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (CDSPlayer) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
	
	@Override
	public int compareTo(CDSPlayer o) {
		if (this.getNumberOfEmeralds() > o.getNumberOfEmeralds()) return -1;
		else if (this.getNumberOfEmeralds() > o.getNumberOfEmeralds()) return 1;
		return 0;
	}
}
