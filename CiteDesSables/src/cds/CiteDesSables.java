package cds;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import commands.BankCommand;
import commands.CDSPlayerCommand;
import commands.ClanCommand;
import commands.HabitationCommand;
import commands.NPCCommand;
import commands.RegionCommand;
import inventory.GUI;
import listeners.BlockBreakListener;
import listeners.ClickListener;
import listeners.EntityDeathListener;
import listeners.FlagsListener;
import listeners.GuiListener;
import listeners.JoinQuitListener;
import listeners.MessageSendListener;
import listeners.RegionListener;
import listeners.SelectionListener;
import listeners.ShopListener;
import managers.ClanManager;
import managers.HabitationManager;
import managers.NPCManager;
import managers.PlayerManager;
import managers.RegionManager;
import scoreboard.ScoreboardManager;

public class CiteDesSables extends JavaPlugin {
	
	private List<Chunk> chunks = new ArrayList<Chunk>();
	private PlayerManager playerManager;
	private ScoreboardManager scoreboardManager;
	private RegionManager regionManager;
	private HabitationManager habitationManager;
	private NPCManager npcManager;
	private ClanManager clanManager;
	private GUI gui;
	public ArrayList<UUID> playerInGame = new ArrayList<>();
	public static final String prefix = "§a[§6Cité des sables§a] §7";
	
	private static CiteDesSables instance;
	
	public void onEnable() {
        registerCommands();
        setInstances();
        registerListeners();
        loadSettings();
        loadChunks(Bukkit.getWorld("world"), 20);
        Bukkit.getWorld("world").setGameRuleValue("reducedDebugInfo", "true");
        Bukkit.getScheduler().runTaskLaterAsynchronously(this, () -> {
        	for (CDSPlayer p : getPlayerManager().getPlayers()) {
            	p.addEmerald();
            	Bukkit.getServer().getPlayer(p.getName()).sendMessage(prefix + "Tu as reçu ton émeraude horaire");
            }
		}, 20*3600);

    }
	
	public void onDisable() {
        System.out.println("CiteDesSables disabled");
        this.playerManager.serializePlayers();
        this.npcManager.serializeNPCManager();
        this.regionManager.saveRegions();
        this.habitationManager.saveHabitations();
        this.clanManager.saveClans();
    }
	
	private void registerCommands() {
		getCommand("classement").setExecutor(new CDSPlayerCommand());
		getCommand("region").setExecutor(new RegionCommand());
		getCommand("habitation").setExecutor(new HabitationCommand());
		getCommand("npc").setExecutor(new NPCCommand());
		getCommand("clan").setExecutor(new ClanCommand());
		getCommand("banque").setExecutor(new BankCommand());
    }
	
	private void loadSettings() {
        Bukkit.getWorld("world").setPVP(false);
        Bukkit.getWorld("world").setGameRuleValue("naturalRegeneration", "true");
        Bukkit.getWorld("world").setAutoSave(true);
        this.regionManager.initializeRegions();
        this.habitationManager.initializeHabitations();
        this.clanManager.initializeClans();
        this.npcManager.initializeNPCs();
    }
	
	private void setInstances() {
        instance = this;
        (scoreboardManager = new ScoreboardManager(this, "§6Cité du soleil")).runTaskTimerAsynchronously(this, 0, 5);
        this.playerManager = new PlayerManager();
        this.regionManager = new RegionManager();
        this.gui = new GUI();
        this.habitationManager = new HabitationManager();
        this.npcManager = new NPCManager();
        this.clanManager = new ClanManager();
        this.getGUI().registersGUI();
    }
	
	public static CiteDesSables getInstance() {
		return instance;
	}
	
	private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new JoinQuitListener(), this);
        pm.registerEvents(new BlockBreakListener(), this);
        pm.registerEvents(new SelectionListener(), this);
        pm.registerEvents(new RegionListener(), this);
        pm.registerEvents(new ShopListener(), this);
        pm.registerEvents(new ClickListener(), this);
        pm.registerEvents(new FlagsListener(), this);
        pm.registerEvents(new EntityDeathListener(), this);
        pm.registerEvents(new MessageSendListener(), this);
        pm.registerEvents(new GuiListener(), this);
	}

	public void loadChunks(World world, int chunks) {
        Double chunk = (double) (chunks * chunks) / 256.0;
        System.out.println("[ChunkLoader] Counted " + chunk + " Chunks !");
        System.out.println("[ChunkLoader] Loading chunks please wait...");
        int sizeMap = chunks;
        int count = 0;
        int x = (int) (-(double) sizeMap / 2.0);
        while ((double) x < (double) sizeMap / 2.0) {
            int z = (int) (-(double) sizeMap / 2.0);
            while ((double) z < (double) sizeMap / 2.0) {
                Chunk c = world.getChunkAt(new Location(world, (double) x, 0.0, (double) z));
                this.chunks.add(c);
                c.load();
                count++;
                double percent = 100 * count / chunk;
                System.out.println("Chunks loaded " + percent + "%");

                z += 16;
            }
            x += 16;
        }
    }
	
	public PlayerManager getPlayerManager() {
		return this.playerManager;
	}
	
	public ScoreboardManager getScorebardManager() {
		return this.scoreboardManager;
	}
	
	public RegionManager getRegionManager() {
		return this.regionManager;
	}
	
	public HabitationManager getHabitationManager() {
		return this.habitationManager;
	}
	
	public GUI getGUI() {
		return this.gui;
	}
	
	public NPCManager getNPCManager() {
		return this.npcManager;
	}
	
	public ClanManager getClanManager() {
		return this.clanManager;
	}
}
