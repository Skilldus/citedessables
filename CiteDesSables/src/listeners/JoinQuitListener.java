package listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import cds.CiteDesSables;
import tab.Tab;

public class JoinQuitListener implements Listener {
	
	@EventHandler
    public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		Tab.send(p, "§aBienvenue dans la cité du soleil, amuses-toi bien !", "§cCe plugin a été réalisé par Skilldus");
		 if (!CiteDesSables.getInstance().playerInGame.contains(p.getUniqueId())) {
			 CiteDesSables.getInstance().playerInGame.add(p.getUniqueId());
			 CiteDesSables.getInstance().getPlayerManager().addPlayer(p.getName());
         }
		 e.setJoinMessage("§8[§a+§8]§7 " + p.getName() + "§8 (§6" + CiteDesSables.getInstance().playerInGame.size() + "§8/§6" + Bukkit.getServer().getMaxPlayers() + "§8)");
		 if (CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).exists()) {
			 CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).loadConfig();
		 }
		 else {
			 CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).writeToFS();
		 }
	}

	@EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        CiteDesSables.getInstance().playerInGame.remove(p.getUniqueId());
        e.setQuitMessage("§8[§c-§8]§7 " + p.getName() + "§8 (§6" + CiteDesSables.getInstance().playerInGame.size() + "§8/§6" + Bukkit.getServer().getMaxPlayers() + "§8)");
        CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).writeToFS();
    }
	
}
