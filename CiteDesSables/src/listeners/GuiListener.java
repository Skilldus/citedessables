package listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import cds.CiteDesSables;


public class GuiListener implements Listener {
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = (Player) event.getPlayer();
		ItemStack it = p.getItemInHand();
		if (it != null) {
			//check item si besoin
		}
	}
	
	@EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inv = event.getInventory();
        ItemStack current = event.getCurrentItem();
        if (current == null) {
            return;
        }
        CiteDesSables.getInstance().getGUI().getRegisteredMenus().values().stream().filter(menu -> inv.getName().equalsIgnoreCase(menu.getName())).forEach(menu -> {
        menu.onClick(player, inv, current, event.getSlot());
            event.setCancelled(true);
        });
	}
}
