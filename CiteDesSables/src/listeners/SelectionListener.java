package listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import cds.CiteDesSables;
import regions.Selection;

public class SelectionListener implements Listener {

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		ItemStack is = p.getItemInHand();
		if (is != null && is.getItemMeta() != null && is.getItemMeta().getDisplayName() != null) {
			if (is.getItemMeta().getDisplayName().equalsIgnoreCase("§aSelection") && is.getType() == Material.WOOD_AXE) {
				if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
					if (!CiteDesSables.getInstance().getRegionManager().getSelections().containsKey(p.getName())) {
						CiteDesSables.getInstance().getRegionManager().getSelections().put(p.getName(), new Selection(null, null));
					}
					CiteDesSables.getInstance().getRegionManager().getSelections().get(p.getName()).setFirstLocation(e.getClickedBlock().getLocation());
					p.sendMessage(CiteDesSables.prefix + "Premières coordonnées de la sélection : " + e.getClickedBlock().getLocation().getX() + 
					" , " + e.getClickedBlock().getLocation().getY() + " , " + e.getClickedBlock().getLocation().getZ());
				}
				else if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if (!CiteDesSables.getInstance().getRegionManager().getSelections().containsKey(p.getName())) {
						CiteDesSables.getInstance().getRegionManager().getSelections().put(p.getName(), new Selection(null, null));
					}
					CiteDesSables.getInstance().getRegionManager().getSelections().get(p.getName()).setSecondLocation(e.getClickedBlock().getLocation());
					p.sendMessage(CiteDesSables.prefix + "Deuxièmes coordonnées de la sélection : " + e.getClickedBlock().getLocation().getX() + 
							" , " + e.getClickedBlock().getLocation().getY() + " , " + e.getClickedBlock().getLocation().getZ());
				}
			}
		}
	}
	
}
