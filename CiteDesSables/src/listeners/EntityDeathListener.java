package listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

import cds.CiteDesSables;
import npc.CDSVillager;

public class EntityDeathListener implements Listener {

	@EventHandler
	public void onEntityDeath(EntityDeathEvent e) {
		Entity entity = e.getEntity();
		if (e.getEntityType() == EntityType.VILLAGER) {
			Villager villager = (Villager) entity;
			String name = villager.getCustomName();
			CDSVillager cdsv = CiteDesSables.getInstance().getNPCManager().getVillagerByName(name);
			if (cdsv != null) {
				CiteDesSables.getInstance().getNPCManager().removeVillager(name);
				CiteDesSables.getInstance().getNPCManager().serializeNPCManager();
				Bukkit.getServer().broadcastMessage("§6" + cdsv.getName() + " est mort ce soir !");
			}
			else {
				Bukkit.getServer().broadcastMessage("Ce villageois n'est pas un NPC");
			}
		}
	}
}
