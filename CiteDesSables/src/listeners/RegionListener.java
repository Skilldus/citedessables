package listeners;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import cds.CiteDesSables;
import managers.RegionManager;
import regions.Region;

public class RegionListener implements Listener {

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		Location previousLoc = e.getFrom();
		Location nextLoc = e.getTo();
		if (!CiteDesSables.getInstance().getRegionManager().getRegions().isEmpty() && CiteDesSables.getInstance().getRegionManager().getRegions() != null) {
			for (Region r : CiteDesSables.getInstance().getRegionManager().getRegions()) {
				if (!RegionManager.isLocationInRegion(previousLoc, r) && RegionManager.isLocationInRegion(nextLoc, r)) {
					p.sendMessage(r.getEntryMessage());
				}
				else if (RegionManager.isLocationInRegion(previousLoc, r) && !RegionManager.isLocationInRegion(nextLoc, r)) {
					p.sendMessage(r.getExitMessage());
				}
			}
		}
	}
}
