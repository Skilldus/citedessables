package listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import cds.CiteDesSables;
import habitations.Habitation;
import managers.RegionManager;
import regions.Region;

public class ShopListener implements Listener {
	
	@EventHandler
	public void onSignChange(SignChangeEvent e) {
		Player p = e.getPlayer();
		String[] lines = e.getLines();
		List<Block> blocks = new ArrayList<Block>();
		System.out.println(e.getBlock().getType());
		Location loc = e.getBlock().getLocation();
		Region r = null;
		for (Region region : CiteDesSables.getInstance().getRegionManager().getRegions()) {
			if (e.getBlock().getType() == Material.WALL_SIGN) {
				Location loc1 = new Location(loc.getWorld(), loc.getBlockX() + 1, loc.getBlockY(), loc.getBlockZ());
				Location loc2 = new Location(loc.getWorld(), loc.getBlockX() - 1, loc.getBlockY(), loc.getBlockZ());
				Location loc3 = new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ() + 1);
				Location loc4 = new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ() - 1);
				blocks.add(loc1.getBlock());
				blocks.add(loc2.getBlock());
				blocks.add(loc3.getBlock());
				blocks.add(loc4.getBlock());
				for (Block b : blocks) {
					if (b.getType() != Material.AIR) {
						r = region;
					}
				}
				

			}
			if (RegionManager.isLocationInRegion(loc, region)) {
				r = region;
			}
		}
		if (r != null && r.hasHabitation()) {
			Habitation h = CiteDesSables.getInstance().getRegionManager().getRegionByName(r.getName()).getHabitation();
			if (lines[0].equalsIgnoreCase("[CDSVente]")) {
				e.setLine(0, "§aHabitation");
				e.setLine(1, "§6" + r.getName());
				e.setLine(2, "§6Prix : §r" + h.getPrice() + "§2E");
				e.setLine(3, (h.getOwner() != null) ? "§9" + h.getOwner() : "§aDisponible");
				Bukkit.getServer().broadcastMessage(String.valueOf(e.getBlock().getLocation().getX()));
				SignChangeEvent saveEvent = e;
				h.setSign(saveEvent);
				h.writeToFS();
				CiteDesSables.getInstance().getHabitationManager().writeToFS();
				p.sendMessage(CiteDesSables.prefix + "Le panneau de vente a été créé");
			}
		}
	}
	
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		if (e.getInventory().getTitle().equalsIgnoreCase("§aBorne de la banque")) {
			Inventory inv = e.getInventory();
			int amount = 0;
			for (int i = 0; i < inv.getSize(); i++) {
				if (inv.getItem(i) != null) {
					if (inv.getItem(i).getType() == Material.EMERALD) {
						amount += inv.getItem(i).getAmount();
					}
				}
			}
			e.getPlayer().sendMessage(CiteDesSables.prefix + "Vous avez ajouté §a" + amount  + " §2émeraudes §7a votre banque");
			CiteDesSables.getInstance().getPlayerManager().getByName(e.getPlayer().getName()).addFoundsToBank(amount);
		}
		if (e.getInventory().getType() == InventoryType.MERCHANT) {
			String name = ((Villager) e.getInventory().getHolder()).getCustomName();
			for (HumanEntity he : e.getInventory().getViewers()) {
				if (he instanceof Player) {
					Player p = (Player) he;
					p.sendMessage("§6" + name + " : §7C'est un plaisir d'avoir pu échanger avec vous !");
				}
			}
		}
	}
}
