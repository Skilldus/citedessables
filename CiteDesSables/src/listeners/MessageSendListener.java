package listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import cds.CDSPlayer;
import cds.CiteDesSables;

public class MessageSendListener implements Listener {
	
	@EventHandler
	public void onMessageSend(AsyncPlayerChatEvent e) {
		Player player = (Player) e.getPlayer();
		String name = player.getName();
		String message = e.getMessage();
		CDSPlayer cdsp = CiteDesSables.getInstance().getPlayerManager().getByName(name);
		String format = "<prefix><player> §8» §r<message>";
		if (cdsp.hasClan()) {
			format = format.replace("<prefix>", "§6[" + cdsp.getClan().getName() + "]§7 ");
			format = format.replace("<player>", "%1$s");
			format = format.replace("<message>", "%2$s");
		}
		
		else {
			format = format.replace("<player>", "%1$s");
			format = format.replace("<prefix>", "§7");
			format = format.replace("<message>", "%2$s");
		}
		
		e.setFormat(format);
		e.setMessage(message);
	}

}
