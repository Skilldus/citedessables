package listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import cds.CDSPlayer;
import cds.CiteDesSables;

public class BlockBreakListener implements Listener {

    @EventHandler
    public void breakBlock(BlockBreakEvent e) {
    	CDSPlayer p = CiteDesSables.getInstance().getPlayerManager().getByName(e.getPlayer().getName());
        //Location breakLoc = e.getBlock().getLocation();
        switch (e.getBlock().getType()) {
            case EMERALD_ORE:
                p.addEmerald();
                p.getBukkitPlayer().sendMessage(CiteDesSables.prefix + "Vous venez de miner un minerai d'émeraude");
                CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).writeToFS();
                
            default:
                break;
        }
        Player player = e.getPlayer();
		ItemStack is = player.getItemInHand();
		if (is != null && is.getItemMeta() != null && is.getItemMeta().getDisplayName() != null) {
			if (is.getItemMeta().getDisplayName().equalsIgnoreCase("§aSelection") && is.getType() == Material.WOOD_AXE) {
				e.setCancelled(true);
			}
		}
    }
}
