package listeners;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

import cds.CiteDesSables;
import gui.MarchandGUI;
import habitations.Habitation;
import npc.CDSVillager;

public class ClickListener implements Listener {

	@EventHandler
	public void onRightClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
			Block block = e.getClickedBlock();
			if (block.getState() instanceof Sign) {
				Sign sign = (Sign) block.getState();
				Bukkit.getServer().broadcastMessage("SIGN : " + sign.getLine(0));
				Habitation habitation = null;
				for (Habitation h : CiteDesSables.getInstance().getHabitationManager().getHabitations()) {
					if (h.getSign().length != 0) {
						if (h.getSign()[1].equalsIgnoreCase(sign.getLine(1))) {
							habitation = h;
							p.performCommand("habitation infos " + habitation.getRegion().getName());
						}
					}
				}
			}
		}
		if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Block block = e.getClickedBlock();
			if (block.getState() instanceof Sign) {
				Sign sign = (Sign) block.getState();
				Habitation habitation = null;
				for (Habitation h : CiteDesSables.getInstance().getHabitationManager().getHabitations()) {
					if (h.getSign().length != 0) {
						if (h.getSign()[1].equalsIgnoreCase(sign.getLine(1))) {
							habitation = CiteDesSables.getInstance().getRegionManager().getRegionByName(h.getRegion().getName()).getHabitation();
							CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).buyHabitation(habitation);
							habitation.writeToFS();
							CiteDesSables.getInstance().getHabitationManager().writeToFS();
							CiteDesSables.getInstance().getRegionManager().getRegionByName(habitation.getRegion().getName()).writeToFS();
							CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onInteractionWithVillager(PlayerInteractEntityEvent e) {
		Player player = e.getPlayer();
		Entity entity = e.getRightClicked();
		if (entity.getType() == EntityType.VILLAGER) {
			Villager villager = (Villager) entity;
			String name = villager.getCustomName();
			e.setCancelled(true);
			CDSVillager cdsv = CiteDesSables.getInstance().getNPCManager().getVillagerByName(name);
			if (cdsv != null) {
				player.sendMessage("§6" + cdsv.getName() + " :§7" + cdsv.getInteractionText());
				switch (cdsv.getMarketType()) {
					case "Banquier":
						Inventory playerBanqueInventory = Bukkit.createInventory(player, 9, "§aBorne de la banque");
						player.openInventory(playerBanqueInventory);
						break;
					default:
						CiteDesSables.getInstance().getGUI().open(player, MarchandGUI.class);
						break;
				}
			}
			
			else {
				player.sendMessage(CiteDesSables.prefix + "Ce villageois n'est pas un NPC");
				}
			}
		}
	
	@EventHandler
	public void onVillagersInventoryOpen(InventoryOpenEvent event) {
		if (event.getInventory().getHolder() instanceof Villager) {
			event.setCancelled(true);
		}
	}
	
}
