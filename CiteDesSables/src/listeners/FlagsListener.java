package listeners;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;

import cds.CiteDesSables;
import managers.RegionManager;
import regions.Flag;
import regions.Region;

public class FlagsListener implements Listener {

	@EventHandler
	public void onPvP(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player damager = (Player) e.getDamager();
			Player p = (Player) e.getEntity();
			Location loc = p.getLocation();
			for (Region r : CiteDesSables.getInstance().getRegionManager().getRegions()) {
				if (RegionManager.isLocationInRegion(loc, r) && r.hasFlag(Flag.getByName("pvp"))) {
					damager.sendMessage(CiteDesSables.prefix + "§cLe pvp est actuellement désactivé dans cette région");
					e.setCancelled(true);
				}
			}
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		Location loc = e.getBlock().getLocation();
		for (Region r : CiteDesSables.getInstance().getRegionManager().getRegions()) {
			if (RegionManager.isLocationInRegion(loc, r) && r.hasFlag(Flag.getByName("build"))) {
				if (r.getHabitation() != null) {
					if (!r.getHabitation().getOwner().equalsIgnoreCase(p.getName())) {
						p.sendMessage(r.getHabitation().getOwner() + " != " + p.getName());
						p.sendMessage(CiteDesSables.prefix + "§cLe build est désactivé dans cette région");
						e.setCancelled(true);
					}
					
					else {
						e.setCancelled(false);
					}
				}
				
				else if (!r.getOwner().equalsIgnoreCase(p.getName())) {
					p.sendMessage(CiteDesSables.prefix + "§cLe build est désactivé dans cette région");
					e.setCancelled(true);
				}
			}
		}	
	}
	
	@EventHandler
	public void onChestOpenEvent(InventoryOpenEvent e) {
		if (e.getInventory().getType() == InventoryType.CHEST && e.getInventory().getTitle().equalsIgnoreCase(InventoryType.CHEST.getDefaultTitle())) {
			Block block = (Block) e.getInventory().getHolder();
			Location loc = block.getLocation();
			Player p = (Player) e.getPlayer();
			for (Region region : CiteDesSables.getInstance().getRegionManager().getRegions()) {
				if (RegionManager.isLocationInRegion(loc, region)) {
					if (!region.getOwner().equalsIgnoreCase(p.getName())) {
						e.setCancelled(true);
						p.sendMessage(CiteDesSables.prefix + "Tu ne peux pas ouvrir ce coffre");
					}
				}
			}
		}
	}
	
}
