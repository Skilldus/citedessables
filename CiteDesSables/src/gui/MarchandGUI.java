package gui;

import java.util.function.Supplier;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import cds.CiteDesSables;
import inventory.CustomInventory;
import utils.ItemCreator;

public class MarchandGUI implements CustomInventory {

	@Override
	public String getName() {
		return "§6Marchand";
	}

	@Override
	public Supplier<ItemStack[]> getContents(Player player) {
		ItemStack[] slots = new ItemStack[getSlots()];
		for (int i = 0; i < 9 ; i++) slots[i] = new ItemCreator(Material.STAINED_GLASS_PANE).setName("§8").getItem();
		//slots[10] = new ItemCreator(Kit.NINJA.getItem()).setName("§e» §3" + Kit.NINJA.getName()).setTableauLores(Kit.NINJA.getDescription()).getItem();
		//slots[11] = new ItemCreator(Kit.SCOUT.getItem()).setName("§e» §3" + Kit.SCOUT.getName()).setTableauLores(Kit.SCOUT.getDescription()).getItem();
		for (int i = 36; i < 45 ; i++) slots[i] = new ItemCreator(Material.STAINED_GLASS_PANE).setName("§8").getItem();
		return () -> slots;
	}

	@Override
	public void onClick(Player player, Inventory inventory, ItemStack clickedItem, int slot) {
		if (clickedItem.hasItemMeta()) {
			if (clickedItem.getItemMeta().getDisplayName().contains("§e» §3")) {
				//DO
			}
		}
		CiteDesSables.getInstance().getGUI().open(player, MarchandGUI.class);
	}

	@Override
	public int getRows() {
		return 5;
	}

}
