package scoreboard.providers;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import cds.CiteDesSables;
import managers.RegionManager;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import regions.Region;
import scoreboard.ScoreboardProvider;

public class CDSProvider extends ScoreboardProvider {

    private Player player;

    public CDSProvider(Player player) {
        this.player = player;
    }


    @Override
    public List<String> getLines() {
        List<String> lines = new ArrayList<>();
        if (player.isOp()) {
        	lines.add("§6Rank : §4Owner");
        }
        else {
        	lines.add("§6Rank : §7Player");
        }
        lines.add("");
        if (CiteDesSables.getInstance().getPlayerManager().getByName(this.player.getName()).getClan() != null) {
        	lines.add("§6Clan : §a" + CiteDesSables.getInstance().getPlayerManager().getByName(this.player.getName()).getClan().getName());
        }
        
        else {
        	lines.add("§6Clan : §cNone");
        }
        
        lines.add("");
        lines.add("§6Banque : §a" + CiteDesSables.getInstance().getPlayerManager().getByName(this.player.getName()).getBankAmount());
        lines.add("");
        if (!CiteDesSables.getInstance().getRegionManager().getRegions().isEmpty() && CiteDesSables.getInstance().getRegionManager().getRegions() != null) {
			for (Region r : CiteDesSables.getInstance().getRegionManager().getRegions()) {
				if (RegionManager.isLocationInRegion(this.player.getLocation(), r)) {
					if (r.hasHabitation()) {
						lines.add("§6Habitation : §a" + r.getHabitation().getOwner());
						lines.add("");
					}
				}
			}
        }
        lines.add("§6Players : §a" + CiteDesSables.getInstance().playerInGame.size() + "§7/§a" + Bukkit.getServer().getMaxPlayers());
        lines.add("");
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        lines.add("§6TPS : §a" + df.format(MinecraftServer.getServer().recentTps[0]));
        return lines;
    }

    public Player getPlayer() {
        return this.player;
    }
}