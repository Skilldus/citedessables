package scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import scoreboard.providers.CDSProvider;

public class ScoreboardManager extends BukkitRunnable{
	
	private ScoreboardHandler sb;
	private String lines = "§7§m-----------------";


	public ScoreboardManager(Plugin m, String t) {
		this.sb = new ScoreboardHandler(m, t);
	}

	public void run() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			ScoreboardHelper board = this.getSb().getScoreboard(player);
			board.clear();
			board.add(this.lines);
		    if(player == null || getProvider(player) == null || getProvider(player).getLines() == null) continue;
			for (String i : getProvider(player).getLines()) {
				board.add(i);
			}
			board.add(this.lines);
			//board.add("          §cBy Skilldus");
			board.update(player);
		}
	}

	private ScoreboardProvider getProvider(Player player) {
		return new CDSProvider(player);
	}

	public String getLines() {
		return this.lines;
	}

	public void setLines(String lines) {
		this.lines = lines;
	}

	public ScoreboardHandler getSb() {
		return this.sb;
	}

	public void setSb(ScoreboardHandler sb) {
		this.sb = sb;
	}

}
