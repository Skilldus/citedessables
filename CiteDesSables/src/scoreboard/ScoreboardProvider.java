package scoreboard;

import java.util.List;

public abstract class ScoreboardProvider {
	
	public abstract List<String> getLines();

}