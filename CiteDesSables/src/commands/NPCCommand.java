package commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cds.CiteDesSables;
import npc.CDSVillager;

public class NPCCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args) {
		boolean op = sender.isOp();
		if (!(sender instanceof Player)) return false;
		Player p = (Player) sender;
		if (args.length == 0) {
			showHelp(p);
		}
		else if (args.length > 0) {
			String cmdAction = args[0];
			if (cmdAction.equalsIgnoreCase("spawnvillager") && op) {
				if (args.length >= 4) {
					String name = args[1];
					String type = args[2];
					String[] tabText = new String[args.length - 3];
					String text = "";
					for (int i = 3; i < tabText.length + 3; i++) {
						text += " " + args[i];
					}
					CDSVillager villager = new CDSVillager(name, text, p.getLocation(), type);
					villager.spawnVillager();
					p.sendMessage(CiteDesSables.prefix + "Vous avez fait apparaître un villageois de type " + villager.getMarketType());
					CiteDesSables.getInstance().getNPCManager().addVillager(villager);
					CiteDesSables.getInstance().getNPCManager().getVillagerByName(villager.getName()).writeToFS();
					CiteDesSables.getInstance().getNPCManager().serializeNPCManager();
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("sizevillager") && op) {
				p.sendMessage("Size : " + CiteDesSables.getInstance().getNPCManager().getVillagers().size());
			}
			
			else if (cmdAction.equalsIgnoreCase("clearvillager") && op) {
				CiteDesSables.getInstance().getNPCManager().clearVillagers();
				CiteDesSables.getInstance().getNPCManager().serializeNPCManager();
				p.sendMessage(CiteDesSables.prefix + "Tout les villageois on été supprimés");
			}
			
			else {
				showHelp(p);
			}
		}
		
		return false;
	}	
	private void showHelp(Player p) {
		p.sendMessage(CiteDesSables.prefix + "Aide pour la commande 'npc' :");
		p.sendMessage("§7- /npc spawnvillager <nom> <type> <texte>");
	}
	
}
