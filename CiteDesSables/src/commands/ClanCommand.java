package commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cds.CDSPlayer;
import cds.CiteDesSables;
import clan.Clan;
import clan.ClanInvite;

public class ClanCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args) {
		boolean op = sender.isOp();
		if (!(sender instanceof Player)) return false;
		Player p = (Player) sender;
		if (args.length == 0) {
			showHelp(p);
		}
		else if (args.length > 0) {
			String cmdAction = args[0];
			if (cmdAction.equalsIgnoreCase("create") && op) {
				if (args.length == 2) {
					String clanName = args[1];
					if (!CiteDesSables.getInstance().getClanManager().clanExist(clanName)) {
						CDSPlayer cdsp = CiteDesSables.getInstance().getPlayerManager().getByName(p.getName());
						Clan clan = new Clan(clanName, cdsp);
						CiteDesSables.getInstance().getClanManager().addClan(clan);
						cdsp.setClan(clan);
						clan.addMember(cdsp);
						clan.writeToFS();
						CiteDesSables.getInstance().getClanManager().serializeClanManager();
						cdsp.writeToFS();
						p.sendMessage(CiteDesSables.prefix + "Vous avez créé le clan " + clanName);
					}
					
					else {
						p.sendMessage(CiteDesSables.prefix + "Ce clan existe déjà");
					}
				}
				
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("info") && op) {
				if (args.length == 2) {
					String clanName = args[1];
					Clan clan = CiteDesSables.getInstance().getClanManager().getClanByName(clanName);
					CDSPlayer pl = CiteDesSables.getInstance().getPlayerManager().getByName(clanName);
					if (pl != null) {
						if (pl.hasClan()) {
							clanName = pl.getClan().getName();
							p.sendMessage("§7-=-=- §6[CLAN §a" + clanName + "§6]§7 -=-=-");
							p.sendMessage("§7Taille : §a(" + pl.getClan().getPlayers().size() + "/" + Clan.getMaxPlayers() + ")");
							p.sendMessage("§7Émeraudes : §a" + pl.getClan().getEmeraldsOfClan());
							p.sendMessage("§7Liste des membres :");
							p.sendMessage(Bukkit.getServer().getPlayer(pl.getClan().getOwner().getName()).isOnline() ? "§a- " + pl.getClan().getOwner().getName() + " §f(Owner)" : "§c- " + clan.getOwner().getName());
							for (CDSPlayer cdsp : pl.getClan().getPlayers()) {
								if (!cdsp.getName().equalsIgnoreCase(pl.getClan().getOwner().getName())) {
									p.sendMessage(Bukkit.getServer().getPlayer(cdsp.getName()).isOnline() ? "§a- " + cdsp.getName() + " §f(Member)" : "§c- " + cdsp.getName());
								}
							}
						}
						
						else {
							p.sendMessage(CiteDesSables.prefix + "Ce joueur n'a pas de clan");
						}
					}
					else if (clan != null) {
						p.sendMessage("§7-=-=- §6[CLAN §a" + clanName + "§6]§7 -=-=-");
						p.sendMessage("§7Taille : §a(" + clan.getPlayers().size() + "/" + Clan.getMaxPlayers() + ")");
						p.sendMessage("§7Émeraudes : §a" + clan.getEmeraldsOfClan());
						p.sendMessage("§7Liste des membres :");
						p.sendMessage(Bukkit.getServer().getPlayer(clan.getOwner().getName()).isOnline() ? "§a- " + clan.getOwner().getName() + " §f(Owner)" : "§c- " + clan.getOwner().getName());
						for (CDSPlayer cdsp : clan.getPlayers()) {
							if (!cdsp.getName().equalsIgnoreCase(clan.getOwner().getName())) {
								p.sendMessage(Bukkit.getServer().getPlayer(cdsp.getName()).isOnline() ? "§a- " + cdsp.getName() + " §f(Member)" : "§c- " + cdsp.getName());
							}
						}
					}
					
					else {
						p.sendMessage(CiteDesSables.prefix + "Ce clan n'existe pas");
					}
				}
				
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("invite") && op) {
				if (args.length == 2) {
					String playerName = args[1];
					CDSPlayer pl = CiteDesSables.getInstance().getPlayerManager().getByName(playerName);
					if (CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).hasClan()) {
						if (pl != null) {
							if (!pl.hasClan()) {
								if (!p.getName().equalsIgnoreCase(playerName)) {
									new ClanInvite(CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).getClan(), pl);
								}
							
								else {
									p.sendMessage(CiteDesSables.prefix + "Tu ne peux pas t'inviter dans ton propre clan");
								}
							}
							
							else {
								p.sendMessage(CiteDesSables.prefix + "Ce joueur possède déjà un clan");
							}
						}
						else {
							p.sendMessage(CiteDesSables.prefix + "Ce joueur n'est pas connecté");
						}
					}
					
					else {
						p.sendMessage(CiteDesSables.prefix + "Tu dois avoir un clan pour pouvoir effectuer cette commande");
					}
				}
				
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("accept") && op) {
				CDSPlayer cdsp = CiteDesSables.getInstance().getPlayerManager().getByName(p.getName());
				if (cdsp.getClan() == null) {	
					if (cdsp.getClanInvite() != null) {
						cdsp.acceptClanInvite();
						Bukkit.getServer().getPlayer(cdsp.getName()).sendMessage(CiteDesSables.prefix + "Vous avez accepté l'invitation du clan " + cdsp.getClan().getName());
						CiteDesSables.getInstance().getClanManager().getClanByName(cdsp.getClan().getName()).writeToFS();
						CiteDesSables.getInstance().getClanManager().serializeClanManager();
						cdsp.writeToFS();
					}
					
					else {
						p.sendMessage(CiteDesSables.prefix + "Vous n'avez reçu aucune invitation récemment");
					}
				}
				
				else {
					p.sendMessage(CiteDesSables.prefix + "Vous avez déjà un clan");
				}
				
			}
			
			else if (cmdAction.equalsIgnoreCase("kick") && op) {
				if (args.length == 2) {
					String playerName = args[1];
					CDSPlayer cdsp = CiteDesSables.getInstance().getPlayerManager().getByName(p.getName());
					CDSPlayer member = CiteDesSables.getInstance().getPlayerManager().getByName(playerName);
					if (cdsp.hasClan()) {
						if (cdsp.getClan().getOwner().getName().equalsIgnoreCase(cdsp.getName())) {
							cdsp.getClan().removeMember(member);
							p.sendMessage(CiteDesSables.prefix + "§c" + playerName + " a été exclu du clan");
							member.setClan(null);
							CiteDesSables.getInstance().getClanManager().getClanByName(cdsp.getClan().getName()).writeToFS();
							CiteDesSables.getInstance().getClanManager().serializeClanManager();
							CiteDesSables.getInstance().getPlayerManager().serializePlayers();
							Bukkit.getServer().getPlayer(playerName).sendMessage(CiteDesSables.prefix + "§cVous avez été exclu de votre clan");
						}
						
						else {
							p.sendMessage(CiteDesSables.prefix + "Tu dois être owner du clan pour pouvoir effectuer cette commande");
						}
					}
					
					else {
						p.sendMessage(CiteDesSables.prefix + "Tu dois avoir un clan pour pouvoir effectuer cette commande");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("disband") && op) {
				if (args.length == 1) {
					CDSPlayer cdsp = CiteDesSables.getInstance().getPlayerManager().getByName(p.getName());
					if (cdsp.hasClan()) {
						if (cdsp.getClan().getOwner().getName().equalsIgnoreCase(cdsp.getName())) {
							CiteDesSables.getInstance().getClanManager().removeClan(cdsp.getClan().getName());
							CiteDesSables.getInstance().getClanManager().serializeClanManager();
							cdsp.setClan(null);
							cdsp.writeToFS();
							Bukkit.getServer().getPlayer(cdsp.getName()).sendMessage(CiteDesSables.prefix + "§cVous avez été disband votre clan");
						}
						
						else {
							p.sendMessage(CiteDesSables.prefix + "Tu dois être owner du clan pour pouvoir effectuer cette commande");
						}
					}
					
					else {
						p.sendMessage(CiteDesSables.prefix + "Tu dois avoir un clan pour pouvoir effectuer cette commande");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("leave") && op) {
				CDSPlayer cdsp = CiteDesSables.getInstance().getPlayerManager().getByName(p.getName());
				if (cdsp.hasClan()) {
					cdsp.getClan().removeMember(cdsp);
					CiteDesSables.getInstance().getClanManager().getClanByName(cdsp.getClan().getName()).writeToFS();
					CiteDesSables.getInstance().getClanManager().serializeClanManager();
					cdsp.setClan(null);
					cdsp.writeToFS();
					p.sendMessage(CiteDesSables.prefix + "§cVous avez quitté votre clan");
				}
				
				else {
					p.sendMessage(CiteDesSables.prefix + "Vous n'avez pas de clan");
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("remove") && op) {
				if (args.length == 2) {
					String clanName = args[1];
					if (CiteDesSables.getInstance().getClanManager().clanExist(clanName)) {
						CiteDesSables.getInstance().getClanManager().removeClan(clanName);
						p.sendMessage(CiteDesSables.prefix + "Le clan " + clanName + " a été supprimé");
						CiteDesSables.getInstance().getClanManager().serializeClanManager();
						CiteDesSables.getInstance().getPlayerManager().serializePlayers();
					}
					
					else {
						p.sendMessage(CiteDesSables.prefix + "Ce clan n'existe pas");
					}
				}
				
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("list") && op) {
				p.sendMessage(CiteDesSables.prefix + "Liste des clans :");
				for (Clan clan : CiteDesSables.getInstance().getClanManager().getClans()) {
					p.sendMessage("§7- " + clan.getName());
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("classement")) {
				CiteDesSables.getInstance().getClanManager().sendLeaderboard(p);
			}
			
			else {
				showHelp(p);
			}
			
		}		
		
		return false;
	}	
	private void showHelp(Player p) {
		p.sendMessage(CiteDesSables.prefix + "Aide pour la commande 'clan' :");
		p.sendMessage("§7- /clan create <nom>");
		p.sendMessage("§7- /clan info <nom/joueur>");
		p.sendMessage("§7- /clan invite <joueur>");
		p.sendMessage("§7- /clan accept");
		p.sendMessage("§7- /clan kick <joueur>");
		p.sendMessage("§7- /clan remove <clan>");
		p.sendMessage("§7- /clan leave");
		p.sendMessage("§7- /clan disband");
		p.sendMessage("§7- /clan list");
		p.sendMessage("§7- /clan classement");
	}
}
