package commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import cds.CiteDesSables;
import regions.Area;
import regions.Flag;
import regions.Region;

public class RegionCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args) {
		boolean op = sender.isOp();
		if (!(sender instanceof Player)) return false;
		Player p = (Player) sender;
		if (args.length == 0) {
			showHelp(p);
		}
		else if (args.length > 0) {
			String cmdAction = args[0];
			if (cmdAction.equalsIgnoreCase("create") && op) {
				if (args.length == 2) {
					String name = args[1];
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
					if (region == null) {
						Region newRegion = new Region(name, p.getName());
						CiteDesSables.getInstance().getRegionManager().addRegion(newRegion);
						newRegion.writeToFS();
						p.sendMessage(CiteDesSables.prefix + "La region §r" + name + " §7a été créée");
						CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette région existe déjà");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("addselection") && op) {
				if (args.length == 2) {
					String name = args[1];
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
					if (region != null) {
						if (CiteDesSables.getInstance().getRegionManager().getSelections().containsKey(p.getName())) {
							Location l1 = CiteDesSables.getInstance().getRegionManager().getSelections().get(p.getName()).getFirstLocation();
							Location l2 = CiteDesSables.getInstance().getRegionManager().getSelections().get(p.getName()).getSecondLocation();
							Area area = new Area(l1, l2);
							region.addArea(area);
							p.sendMessage(CiteDesSables.prefix + "Une nouvelle selection a été rajoutée à la région " + region.getName());
							CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
						}
						else {
							p.sendMessage(CiteDesSables.prefix + "Il faut d'abord sélectionner une zone");
						}
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette région n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("removeselection") && op) {
				if (args.length == 2) {
					String name = args[1];
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
					if (region != null) {
						region.removeArea(region.getAreas().get(region.getAreas().size() - 1));
						p.sendMessage(CiteDesSables.prefix + "La dernière sélection de la région " + region.getName() + " a été retirée");
						CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette région n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("addflag") && op) {
				if (args.length == 3) {
					String name = args[1];
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
					if (region != null) {
						region.removeArea(region.getAreas().get(region.getAreas().size() - 1));
						p.sendMessage(CiteDesSables.prefix + "La dernière sélection de la région " + region.getName() + " a été retirée");
						CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette région n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("remove") && op) {
				if (args.length == 2) {
					String name = args[1];
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
					if (region != null) {
						CiteDesSables.getInstance().getHabitationManager().removeHabitation((region.getHabitation()));
						CiteDesSables.getInstance().getRegionManager().removeRegion(region);
						p.sendMessage(CiteDesSables.prefix + "La région §r" + name + " §7a été supprimée");
						CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette région n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("tool") && op) {
				ItemStack is = new ItemStack(Material.WOOD_AXE, 1);
				ItemMeta im = is.getItemMeta();
				im.setDisplayName("§aSelection");
				is.setItemMeta(im);
				p.getInventory().addItem(is);
				p.sendMessage(CiteDesSables.prefix + "Tu as reçu l'outil de sélection de région");
			}
			
			else if (cmdAction.equalsIgnoreCase("list") && op) {
				p.sendMessage(CiteDesSables.prefix + "Liste des régions : (" + CiteDesSables.getInstance().getRegionManager().getRegions().size() + ")");
				for (Region r : CiteDesSables.getInstance().getRegionManager().getRegions()) {
					p.sendMessage("§7- " + r.getName());
				}
			}
				
			else if (cmdAction.equalsIgnoreCase("tp") && op) {
				if (args.length == 2) {
					String name = args[1];
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
					if (region != null) {
						p.teleport(new Location(Bukkit.getServer().getWorld(region.getAreas().get(0).getFirstLocationWorld()), region.getAreas().get(0).getFirstLocationX(), region.getAreas().get(0).getFirstLocationY(), region.getAreas().get(0).getFirstLocationZ()));
						p.sendMessage(CiteDesSables.prefix + "Vous avez été téléporté à la région §r" + region.getName());
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette région n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("infos") && op) {
				if (args.length == 2) {
					String name = args[1];
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
					if (region != null) {
						p.sendMessage(CiteDesSables.prefix + "Informations de la région §r" + region.getName());
						p.sendMessage("§7Nom : §a" + region.getName());
						p.sendMessage("§7Propriétaire : §a" + region.getOwner());
						p.sendMessage("§7Location : §a" + region.getAreas().get(0).getFirstLocationX() + ", " + region.getAreas().get(0).getFirstLocationY() + ", " + region.getAreas().get(0).getFirstLocationZ());
						p.sendMessage("§7Monde : §a" + region.getAreas().get(0).getFirstLocationWorld());
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette région n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("flags") && op) {
				if (args.length == 4) {
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(args[2]);
					if (region != null ) {
						if (args[1].equalsIgnoreCase("add")) {
							Flag flag = Flag.getByName(args[3]);
							if (flag != null) {
								if (!region.hasFlag(flag)) {
									region.addFlag(flag);
									region.writeToFS();
									CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
									p.sendMessage(CiteDesSables.prefix + "Le flag " + flag.getName() + " a été rajouté à la region " + region.getName());
								}
								else {
									p.sendMessage(CiteDesSables.prefix + "Ce flag est déjà dans la région");
								}
							}
							else {
								p.sendMessage(CiteDesSables.prefix + "Le flag " + args[3] + " n'existe pas");
							}
						}
						else if (args[1].equalsIgnoreCase("remove")) {
							Flag flag = Flag.getByName(args[3]);
							if (flag != null) {
								if (region.hasFlag(flag)) {
									region.removeFlag(flag);
									region.writeToFS();
									CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
									p.sendMessage(CiteDesSables.prefix + "Le flag " + flag.getName() + " a été retiré à la region " + region.getName());
								}
								else {
									p.sendMessage(CiteDesSables.prefix + "Ce flag n'est pas présent dans la region");
									}
							}
							else {
								p.sendMessage(CiteDesSables.prefix + "Le flag " + args[3] + "n'existe pas");
							}
						}
						else {
							showHelp(p);
						}
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette region n'existe pas");
					}
				}
				else if (args.length == 3) {
					Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(args[2]);
					if (region != null)  {
						if (args[1].equalsIgnoreCase("list")) {
							p.sendMessage(CiteDesSables.prefix + "Liste des flags de la region " + region.getName() + ":");
							for (Flag flag : region.getFlags()) {
								p.sendMessage("§7- " + flag.getName());
							}
						}
						else {
							showHelp(p);
							}
						}
					else {
						showHelp(p);
					}
				}
				else {
					showHelp(p);
				}
			}
			else {
				showHelp(p);
			}
		}
		
		return false;
	}	
	private void showHelp(Player p) {
		p.sendMessage(CiteDesSables.prefix + "Aide pour la commande 'region' :");
		p.sendMessage("§7- /region create <region>");
		p.sendMessage("§7- /region remove <region>");
		p.sendMessage("§7- /region list");
		p.sendMessage("§7- /region tool");
		p.sendMessage("§7- /region infos <region>");
		p.sendMessage("§7- /region tp <region>");
		p.sendMessage("§7- /region flags add <region> <flag>");
		p.sendMessage("§7- /region flags list <region>");
	}
	
}
