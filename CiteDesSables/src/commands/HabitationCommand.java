package commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cds.CiteDesSables;
import habitations.Habitation;
import regions.Region;

public class HabitationCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args) {
		boolean op = sender.isOp();
		if (!(sender instanceof Player)) return false;
		Player p = (Player) sender;
		if (args.length == 0) {
			showHelp(p);
		}
		else if (args.length > 0) {
			String cmdAction = args[0];
			if (cmdAction.equalsIgnoreCase("create") && op) {
				if (args.length == 3) {
					String name = args[1];
					int price = Integer.parseInt(args[2]);
					Habitation h = CiteDesSables.getInstance().getHabitationManager().getHabitationByName(name);
					if (h == null) {
						Region region = CiteDesSables.getInstance().getRegionManager().getRegionByName(name);
						if (!region.getAreas().isEmpty()) {
							Habitation newHabitation = new Habitation(region, price);
							CiteDesSables.getInstance().getRegionManager().getRegionByName(name).setHabitation(newHabitation);
							newHabitation.writeToFS();
							CiteDesSables.getInstance().getHabitationManager().addHabitation(newHabitation);
							p.sendMessage(CiteDesSables.prefix + "Vous avez créé une nouvelle habitation (" + newHabitation.getRegion().getName() + ")");
							CiteDesSables.getInstance().getRegionManager().getRegionByName(name).writeToFS();
							CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
							CiteDesSables.getInstance().getHabitationManager().writeToFS();
						}
						else {
							p.sendMessage(CiteDesSables.prefix + "La région doit avoir au minimum une area");
						}
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("remove") && op) {
				if (args.length == 2) {
					String name = args[1];
					Habitation h = CiteDesSables.getInstance().getHabitationManager().getHabitationByName(name);
					if (h != null) {
						CiteDesSables.getInstance().getHabitationManager().removeHabitation(h);
						CiteDesSables.getInstance().getRegionManager().getRegionByName(h.getRegion().getName()).setHabitation(null);
						CiteDesSables.getInstance().getRegionManager().getRegionByName(h.getRegion().getName()).writeToFS();
						p.sendMessage(CiteDesSables.prefix + "L'habitation de la région " + h.getRegion().getName() + " appartenant à " + h.getOwner()
						+ " a été supprimée");
						CiteDesSables.getInstance().getHabitationManager().writeToFS();
						CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette habitation n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("infos") && op) {
				if (args.length == 2) {
					String name = args[1];
					Habitation h = CiteDesSables.getInstance().getRegionManager().getRegionByName(name).getHabitation();
					if (h != null) {
						p.sendMessage(CiteDesSables.prefix + "Informations de l'habitation §r" + h.getRegion().getName());
						p.sendMessage("§7Nom : §a" + h.getRegion().getName());
						p.sendMessage("§7Propriétaire : §a" + h.getOwner());
						p.sendMessage("§7Prix : §a" + h.getPrice() + " §2émeraudes");
						p.sendMessage("§7Monde : §a" + h.getRegion().getAreas().get(0).getFirstLocationWorld());
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Cette habitation n'existe pas");
					}
				}
				else {
					showHelp(p);
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("list") && op) {
				p.sendMessage(CiteDesSables.prefix + "Liste des habitations : (" + CiteDesSables.getInstance().getHabitationManager().getHabitations().size() + ")");
				for (Habitation h : CiteDesSables.getInstance().getHabitationManager().getHabitations()) {
					p.sendMessage("§7- " + h.getRegion().getName());
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("setowner") && op) {
				if (args.length == 3) {
					String name = args[1];
					String playerName = args[2];
					if (CiteDesSables.getInstance().getPlayerManager().getByName(playerName) != null) {
						Habitation h = CiteDesSables.getInstance().getRegionManager().getRegionByName(name).getHabitation();
						h.setOwner(playerName);
						h.writeToFS();
						CiteDesSables.getInstance().getHabitationManager().writeToFS();
						CiteDesSables.getInstance().getRegionManager().getRegionByName(name).writeToFS();
						CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
						p.sendMessage(CiteDesSables.prefix + "Le nouveau propriétaire de " + h.getRegion().getName() + " est maintenant " + playerName);
					}
					else {
						p.sendMessage(CiteDesSables.prefix + "Le joueur " + playerName + " n'existe pas.");
					}
				}
				else {
					p.sendMessage(CiteDesSables.prefix + "Cette habitation n'existe pas");
				}
			}
			
			else if (cmdAction.equalsIgnoreCase("removeowner") && op) {
				if (args.length == 2) {
					String name = args[1];
					Habitation h = CiteDesSables.getInstance().getRegionManager().getRegionByName(name).getHabitation();
					h.removeOwner();
					CiteDesSables.getInstance().getRegionManager().getRegionByName(name).writeToFS();
					CiteDesSables.getInstance().getRegionManager().serializeRegionManager();
					h.writeToFS();
					CiteDesSables.getInstance().getHabitationManager().writeToFS();
					p.sendMessage(CiteDesSables.prefix + "L'owner de cette habitation a été retiré");
				}
				else {
					p.sendMessage(CiteDesSables.prefix + "Cette habitation n'existe pas");
				}
			}
			
			else {
				showHelp(p);
			}
		}
		
		return false;
	}	
	private void showHelp(Player p) {
		p.sendMessage(CiteDesSables.prefix + "Aide pour la commande 'habitation' :");
		p.sendMessage("§7- /habitation create <region> <prix>");
		p.sendMessage("§7- /habitation remove <region>");
		p.sendMessage("§7- /habitation infos <region>");
		p.sendMessage("§7- /habitation setowner <region> <owner>");
		p.sendMessage("§7- /habitation removeowner <region>");
	}
}
