package commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cds.CiteDesSables;

public class CDSPlayerCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args) {
		if(!sender.isOp()) return false;
		if (!(sender instanceof Player)) return false;
		Player p = (Player) sender;
		p.sendMessage(CiteDesSables.prefix + "Time played : " + p.getTicksLived() / 20);
		CiteDesSables.getInstance().getPlayerManager().sendLeaderboard(p);
		return false;
	}
}