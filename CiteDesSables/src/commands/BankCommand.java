package commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cds.CiteDesSables;

public class BankCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String [] args) {
		if(!sender.isOp()) return false;
		if (!(sender instanceof Player)) return false;
		Player p = (Player) sender;
		if (args.length == 0) {
			showHelp(p);
		}
		
		else if (args.length > 0) {
			String cmdAction = args[0];
			if (cmdAction.equalsIgnoreCase("see")) {
				p.sendMessage(CiteDesSables.prefix + "Compte : §a" + CiteDesSables.getInstance().getPlayerManager().getByName(p.getName()).getBankAmount() + " §2émeraudes");
			}
			
			else {
				showHelp(p);
			}
		}
		
		else {
			showHelp(p);
		}
		
		return false;
	}
	
	private void showHelp(Player p) {
		p.sendMessage(CiteDesSables.prefix + "Aide pour la commande 'banque' :");
		p.sendMessage("§7- /banque see");
	}
}