package habitations;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.block.SignChangeEvent;

import regions.Region;

public class Habitation implements Serializable {

	private static final long serialVersionUID = -3989770366042678651L;
	private Region region;
	private String owner;
	private int price;
	private boolean buyable;
	private String signLines[];
	private double signLocation[];
	
	public Habitation(Region region, int price) {
		this.region = region;
		this.owner = null;
		this.price = price;
		this.buyable = true;
		this.signLines = new String[4];
	}
	
	public Region getRegion() {
		return this.region;
	}
	
	public String getOwner() {
		return this.owner;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public boolean isBuyable() {
		return this.buyable;
	}
	
	public void setSign(SignChangeEvent sign) {
		this.signLocation = new double[3];
		for (int i = 0; i < sign.getLines().length; i++) {
			this.signLines[i] = sign.getLine(i);
		}
		this.signLocation[0] = sign.getBlock().getLocation().getX();
		this.signLocation[1] = sign.getBlock().getLocation().getY();
		this.signLocation[2] = sign.getBlock().getLocation().getZ();
	}
	
	public String[] getSign() {
		return this.signLines;
	}
	
	public double[] getSignLocation() {
		return this.signLocation;
	}
	
	public void removeOwner() {
		this.owner = null;
		this.signLines[3] = "§aDisponible";
		Location loc = new Location(Bukkit.getServer().getWorld("world"), this.signLocation[0], this.signLocation[1], this.signLocation[2]);
		Block block = loc.getBlock();
		if (block != null) {
			Sign sign = (Sign) block.getState();
			sign.setLine(3, this.signLines[3]);
			sign.update();
		}
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
		this.signLines[3] = "§9" + owner;
		Location loc = new Location(Bukkit.getServer().getWorld("world"), this.signLocation[0], this.signLocation[1], this.signLocation[2]);
		Block block = loc.getBlock();
		if (block != null) {
			if (block.getType() == Material.SIGN_POST || block.getType() == Material.WALL_SIGN) {
				Sign sign = (Sign) block.getState();
				sign.setLine(3, this.signLines[3]);
				sign.update();
			}
		}
		else {
			System.out.println("Block Sign null");
		}
		
	}
	
	public void writeToFS() {
		File dir = new File("plugins/CiteDesSables/habitations/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/habitations/" + this.region.getName() + ".data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : " + this.region.getName() + ".data");
			e.printStackTrace();
		}
	}
	
	public Habitation readFromFS() {
		Habitation ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/habitations/" + this.region.getName() + ".data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (Habitation) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
}
