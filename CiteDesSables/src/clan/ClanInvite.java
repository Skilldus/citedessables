package clan;

import org.bukkit.Bukkit;

import cds.CDSPlayer;
import cds.CiteDesSables;

public class ClanInvite {

	private Clan from;
	private CDSPlayer to;
	
	public ClanInvite(Clan from, CDSPlayer to) {
		this.from = from;
		this.to = to;
		for (CDSPlayer p : from.getPlayers()) {
			Bukkit.getServer().getPlayer(p.getName()).sendMessage(CiteDesSables.prefix + "§aLe joueur " + to.getName() + " a été invité à rejoindre le clan");
		}
		Bukkit.getServer().getPlayer(to.getName()).sendMessage(CiteDesSables.prefix + "§aLe clan " + from.getName() + " t'as invité à le rejoindre");
		Bukkit.getServer().getPlayer(to.getName()).sendMessage(CiteDesSables.prefix + "§aUtilise la commande §7/clan accept §apour accepter la requête");
		to.setClanInvite(this);
		Bukkit.getScheduler().runTaskLaterAsynchronously(CiteDesSables.getInstance(), () -> {
			if (to.getClanInvite() != null) {
				to.setClanInvite(null);
				Bukkit.getServer().getPlayer(to.getName()).sendMessage(CiteDesSables.prefix + "§cLa requête du clan " + from.getName() + " a expiré");
				for (CDSPlayer p : from.getPlayers()) {
					Bukkit.getServer().getPlayer(p.getName()).sendMessage(CiteDesSables.prefix + "§cL'invitation du joueur " + to.getName() + " a rejoindre le clan a expiré");
				}
			}
		}, 1000);

	}
	
	public Clan getClan() {
		return this.from;
	}
	
	public CDSPlayer getPlayer() {
		return this.to;
	}
	
}
