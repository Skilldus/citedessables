package clan;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cds.CDSPlayer;

public class Clan implements Serializable, Comparable<Clan> {

	private static final long serialVersionUID = 4409978253350716730L;
	private String name;
	private CDSPlayer owner;
	private List<CDSPlayer> players;
	private static final int maxPlayers = 10;
	
	
	public Clan(String name, CDSPlayer owner) {
		this.name = name;
		this.owner = owner;
		this.players = new ArrayList<CDSPlayer>();
	}
	
	public String getName() {
		return this.name;
	}
	
	public List<CDSPlayer> getPlayers() {
		return this.players;
	}
	
	public boolean addMember(CDSPlayer p) {
		boolean b = false;
		if (this.players.size() < maxPlayers) {
			b = this.players.add(p);
		}
		return b;
	}
	
	public void invitePlayer(CDSPlayer p) {
		
	}
	
	public boolean hasPlayer(CDSPlayer p) {
		return this.players.contains(p);
	}
	
	public boolean removeMember(CDSPlayer p) {
		return this.players.remove(p);
	}
	
	public CDSPlayer getOwner() {
		return this.owner;
	}
	
	public static int getMaxPlayers() {
		return maxPlayers;
	}
	
	public int getEmeraldsOfClan() {
		int em = 0;
		for (CDSPlayer cdsp : this.players) {
			em += cdsp.getNumberOfEmeralds();
		}
		return em;
	}
	
	public void writeToFS() {
		File dir = new File("plugins/CiteDesSables/clans/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/clans/" + this.getName() + ".data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
            System.out.println("CLANS : " + this.name + " has been saved");
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : " + this.getName() + ".data");
			e.printStackTrace();
		}
	}
	
	
	
	public Clan readFromFS() {
		Clan ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/clans/" + this.getName() + ".data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (Clan) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}

	@Override
	public int compareTo(Clan o) {
		if (this.getEmeraldsOfClan() > o.getEmeraldsOfClan()) return -1;
		else if (this.getEmeraldsOfClan() > o.getEmeraldsOfClan()) return 1;
		return 0;
	}
	
}
