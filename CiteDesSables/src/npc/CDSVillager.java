package npc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Villager;

public class CDSVillager implements Serializable {

	private static final long serialVersionUID = 694574739015703053L;
	private static final String[] types = {"Marchand", "Armurier", "SDF", "Banquier"};
	private String name;
	private String interactionText;
	private transient Location location;
	private double locX;
	private double locY;
	private double locZ;
	private String locWorld;
	private String marketType;
	
	public CDSVillager(String name, String interactionText, Location location, String marketType) {
		this.name = name;
		this.interactionText = interactionText;
		this.location = location;
		this.locX = location.getX();
		this.locY = location.getY();
		this.locZ = location.getZ();
		this.locWorld = location.getWorld().getName();
		if (this.location != null) {
			this.locX = location.getX();
			this.locY = location.getY();
			this.locZ = location.getZ();
			this.locWorld = location.getWorld().getName();
		}
		int i = 0;
		boolean typeFind = false;
		while (i < types.length && !typeFind) {
			if (marketType.equalsIgnoreCase(types[i])) {
				this.marketType = marketType;
				typeFind = true;
			}
			i++;
		}
		if (!typeFind) this.marketType = types[0];
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getInteractionText() {
		return this.interactionText;
	}
	
	public Location getLocation() {
		return this.location;
	}

	public double getLocX() {
		return this.locX;
	}
	
	public double getLocY() {
		return this.locY;
	}
	
	public double getLocZ() {
		return this.locZ;
	}
	
	public String getWorldName() {
		return this.locWorld;
	}
	
	public void spawnVillager() {
		Villager npc = Bukkit.getServer().getWorld(this.locWorld).spawn(this.location, Villager.class);
		//net.minecraft.server.v1_8_R3.EntityVillager nmsVillager = ((CraftVillager) npc).getHandle();
		npc.setCustomName(this.name);
		/*try
	    {
			if (!this.getMarketType().equalsIgnoreCase("Banquier")) {
		      Field recipes = nmsVillager.getClass().getDeclaredField("br");
		      recipes.setAccessible(true);
		      MerchantRecipeList list = new MerchantRecipeList();
		      ItemStack in = new ItemStack(Material.STICK, 1);
		      ItemStack in2 = new ItemStack(Material.DIAMOND, 2);
		      ItemStack out = new ItemStack(Material.DIAMOND_SWORD, 1);
		      MerchantRecipe trade = new MerchantRecipe(CraftItemStack.asNMSCopy(in), CraftItemStack.asNMSCopy(in2), CraftItemStack.asNMSCopy(out));
		      list.add(trade);    
		      recipes.set(nmsVillager, list);
			}
	      
	    } catch (Exception e) { e.printStackTrace(); }*/
		
	}
	
	public static String[] getTypes() {
		return types;
	}
	
	public String getMarketType() {
		return this.marketType;
	}
	
	public void writeToFS() {
		File dir = new File("plugins/CiteDesSables/npc/villagers");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/npc/villagers/" + this.getName() + "_" + this.getMarketType() + ".data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : " + this.getName() + ".data");
			e.printStackTrace();
		}
	}
	
	public CDSVillager readFromFS() {
		CDSVillager ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/npc/villagers/" + this.getName() + "_" + this.getMarketType() + ".data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (CDSVillager) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
}
