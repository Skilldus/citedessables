package managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import npc.CDSVillager;

public class NPCManager implements Serializable {
	
	private static final long serialVersionUID = -4423321877517904106L;
	private Set<CDSVillager> villagers = new HashSet<>();

	public NPCManager() {
		File dir = new File("plugins/CiteDesSables/npcManager/");
		if (!dir.exists()) {
			dir.mkdirs();
			this.villagers = new HashSet<CDSVillager>();
		}
		else {
			NPCManager npcManager = readFromFS();
			if (npcManager != null) {
				this.villagers = npcManager.getVillagers();
			}
			else {
				this.villagers = new HashSet<CDSVillager>();
			}
		}
	}
		
	public Set<CDSVillager> getVillagers() {
        return this.villagers;
    }
	
	public CDSVillager getVillagerByName(String vName) {
        return villagers.stream().filter(v -> v.getName().equalsIgnoreCase(vName)).findFirst().orElse(null);
    }
	
	public boolean addVillager(CDSVillager v) {
		return this.villagers.add(v);
	}
	
	public boolean removeVillager(String name) {
		return this.villagers.remove(getVillagerByName(name));
	}
	
	public void clearVillagers() {
		this.villagers.clear();
	}
	
	public void serializeNPCManager() {
		File dir = new File("plugins/CiteDesSables/npcManager/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/npcManager/npcManager.data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : npcManager.data");
			e.printStackTrace();
		}
	}
	
	public NPCManager readFromFS() {
		NPCManager ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/npcManager/npcManager.data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (NPCManager) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
	
	public void initializeNPCs() {
		if (!this.villagers.isEmpty()) {	
			for (CDSVillager cdsv : this.villagers) {
				cdsv = cdsv.readFromFS();
			}
		}
	}
}
