package managers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;

import cds.CDSPlayer;
import cds.CiteDesSables;

public class PlayerManager {
	
	private static PlayerManager instance;
	private Set<CDSPlayer> players = new HashSet<>();

	public PlayerManager() {
		instance = this;
	}
	
	public static PlayerManager getInstance() {
		return instance;
	}
	
	public void addPlayer(String name) {
		CDSPlayer player = new CDSPlayer(name);
		this.players.add(player);
	}
	
	public Collection<CDSPlayer> getPlayers() {
        return this.players;
    }
	
	public void sendInfos() {
		for (CDSPlayer cdsplayer : this.players) {
			cdsplayer.getBukkitPlayer().sendMessage("Tu es dans la liste");
		}
	}
	
	public void sendLeaderboard(Player p) {
		if (!this.players.isEmpty()) {
			File dir = new File("plugins/CiteDesSables/players/");
			CDSPlayer plList[] = new CDSPlayer[dir.list().length];
			ArrayList<CDSPlayer> allPlayers = new ArrayList<CDSPlayer>();
			for (String s : dir.list()) {
				String name = s.replace(".data", "");
				CDSPlayer cdsp = new CDSPlayer(name);
				cdsp.loadConfig();
				allPlayers.add(cdsp);
			}
			for (int i = 0; i < plList.length; i++) {
				plList[i] = allPlayers.get(i);
			}
			Arrays.sort(plList);
			p.sendMessage("§7-=-= §eClassement des joueurs §7=-=-");
			for (int j = 0; j < plList.length; j++) {
				p.sendMessage("§6- " + (j+1) + "§7) §a" + plList[j].getName() + " §2(" + plList[j].getNumberOfEmeralds() + " émeraudes)");
			}
		}
		else {
			p.sendMessage(CiteDesSables.prefix + "§cIl n'y a aucun joueurs existants");
		}
	}
	
	public CDSPlayer getByName(String playerName) {
        return this.players.stream().filter(p -> p.getName().equalsIgnoreCase(playerName)).findFirst().orElse(null);
    }
	
	public boolean existPlayer(String playerName) {
		return this.getByName(playerName) != null;
	}
	
	public void serializePlayers() {
		for (CDSPlayer p : this.players) {
			p.writeToFS();
		}
		System.out.println("Players data have been saved");
	}
}
