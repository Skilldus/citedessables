package managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.entity.Player;

import cds.CDSPlayer;
import cds.CiteDesSables;
import clan.Clan;

public class ClanManager implements Serializable {
	
	private static final long serialVersionUID = -1523735241713774256L;
	private List<Clan> clans;
	
	public ClanManager() {
		File dir = new File("plugins/CiteDesSables/clanManager/");
		if (!dir.exists()) {
			dir.mkdirs();
			this.clans = new ArrayList<Clan>();
		}
		else {
			ClanManager clanManager = readFromFS();
			if (clanManager != null) {
				this.clans = clanManager.getClans();
			}
			else {
				this.clans = new ArrayList<Clan>();
			}
		}
	}
		
	public List<Clan> getClans() {
        return this.clans;
    }
	
	public Clan getClanByName(String cName) {
        return this.clans.stream().filter(c -> c.getName().equalsIgnoreCase(cName)).findFirst().orElse(null);
    }
	
	public boolean addClan(Clan c) {
		return this.clans.add(c);
	}
	
	public void sendLeaderboard(Player p) {
		if (!this.clans.isEmpty()) {
			Clan clanList[] = new Clan[this.clans.size()];
			for (int i = 0; i < clanList.length; i++) {
				clanList[i] = this.clans.get(i);
			}
			Arrays.sort(clanList);
			p.sendMessage("§7-=-= §eClassement des clans §7=-=-");
			for (int i = 0; i < clanList.length; i++) {
				p.sendMessage("§6- " + (i+1) + "§7) §a" + clanList[i].getName());
			}
		}
		else {
			p.sendMessage(CiteDesSables.prefix + "§cIl n'y a aucun clans existants");
		}
	}
	
	public boolean removeClan(String name) {
		Clan c = getClanByName(name);
		try {
			File f = new File("plugins/CiteDesSables/clans/" + c.getName() + ".data");
			if (f.delete()) {
				for (CDSPlayer p : c.getPlayers()) {
					p.setClan(null);
				}
				System.out.println("Le fichier " + c.getName() + ".data a été supprimé");
			} else {
				System.out.println("Erreur lors de la suppression");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.clans.remove(c);
	}
	
	public boolean clanExist(String clan) {
		return getClanByName(clan) != null;
	}
	
	public void serializeClanManager() {
		File dir = new File("plugins/CiteDesSables/clanManager/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/clanManager/clanManager.data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : clanManager.data");
			e.printStackTrace();
		}
	}
	
	public ClanManager readFromFS() {
		ClanManager ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/clanManager/clanManager.data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (ClanManager) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
	
	public void initializeClans() {
		if (!this.clans.isEmpty()) {	
			for (Clan c : this.clans) {
				c = c.readFromFS();
			}
		}
	}
	
	public void saveClans() {
		if (!this.clans.isEmpty()) {	
			for (Clan c : this.clans) {
				c.writeToFS();
			}
		}
	}
}
