package managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import habitations.Habitation;

public class HabitationManager  implements Serializable {

	private static final long serialVersionUID = 2232706555077326523L;
	private List<Habitation> habitations;
	
	public HabitationManager() {
		File dir = new File("plugins/CiteDesSables/habitationManager/");
		if (!dir.exists()) {
			dir.mkdirs();
			this.habitations = new ArrayList<Habitation>();
		}
		else {
			HabitationManager hm = readFromFS();
			if (hm != null) {
				this.habitations = hm.getHabitations();
			}
			else {
				this.habitations = new ArrayList<Habitation>();
			}
		}
	}
	
	public List<Habitation> getHabitations() {
		return this.habitations;
	}
	
	public void initializeHabitations() {
		if (!this.habitations.isEmpty()) {	
			for (Habitation h : this.habitations) {
				h = h.readFromFS();
			}
		}
	}
	
	public void saveHabitations() {
		if (!this.habitations.isEmpty()) {	
			for (Habitation h : this.habitations) {
				h.writeToFS();
			}
		}
	}
	
	public void addHabitation(Habitation h) {
		this.habitations.add(h);
	}
	
	public boolean removeHabitation(Habitation h) {
		try {
			File f = new File("plugins/CiteDesSables/habitations/" + h.getRegion().getName() + ".data");
			if (f.delete()) {
				System.out.println("Le fichier " + h.getRegion().getName() + ".data a été supprimé");
			} else {
				System.out.println("Erreur lors de la suppression");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.habitations.remove(h);
	}
	
	public Habitation getHabitationByName(String name) {
		Habitation h = null;
		int i = 0;
		boolean find = false;
		while (i < this.habitations.size() && !find) {
			if (name.equalsIgnoreCase(this.habitations.get(i).getRegion().getName())) {
				h = this.habitations.get(i);
				find = true;
			}
			i++;
		}
		return h;
	}
	
	public void writeToFS() {
		File dir = new File("plugins/CiteDesSables/habitationManager/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/habitationManager/habitationManager.data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : habitationManager.data");
			e.printStackTrace();
		}
	}
	
	public HabitationManager readFromFS() {
		HabitationManager ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/habitationManager/habitationManager.data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (HabitationManager) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
}
