package managers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;

import cds.CiteDesSables;
import regions.Region;
import regions.Selection;

public class RegionManager implements Serializable {
	
	private static final long serialVersionUID = 195375851131251306L;
	private transient HashMap<String, Selection> selections;
	private List<Region> regions;
	
	public RegionManager() {
		File dir = new File("plugins/CiteDesSables/regionManager/");
		if (!dir.exists()) {
			dir.mkdirs();
			this.regions = new ArrayList<Region>();
		}
		else {
			RegionManager rm = readFromFS();
			if (rm != null) {
				this.regions = rm.getRegions();
			}
			else {
				this.regions = new ArrayList<Region>();
			}
		}
		this.selections = new HashMap<String, Selection>();
	}
	
	public List<Region> getRegions() {
		return this.regions;
	}
	
	public void initializeRegions() {
		if (!this.regions.isEmpty()) {	
			for (Region r : this.regions) {
				r = r.readFromFS();
			}
		}
	}
	
	public void saveRegions() {
		if (!this.regions.isEmpty()) {	
			for (Region r : this.regions) {
				r.writeToFS();
			}
		}
	}
	
	public void addRegion(Region r) {
		this.regions.add(r);
	}
	
	public static boolean isLocationInRegion(Location loc, Region r) {
		boolean isInRegion = false;
		int i = 0;
		while (i < r.getAreas().size() && !isInRegion) {
			double minX = r.getAreas().get(i).getMinLocationX();
			double minY = r.getAreas().get(i).getMinLocationY();
			double minZ = r.getAreas().get(i).getMinLocationZ();
			double maxX = r.getAreas().get(i).getMaxLocationX();
			double maxY = r.getAreas().get(i).getMaxLocationY();
			double maxZ = r.getAreas().get(i).getMaxLocationZ();
			if (loc.getBlockX() >= minX && loc.getBlockX() <= maxX && loc.getBlockY() >= minY && loc.getBlockY() <= maxY+1 && loc.getBlockZ() >= minZ && loc.getBlockZ() <= maxZ) {
				isInRegion = true;
			}
			i++;
		}
		return isInRegion;
	}
	
	public boolean removeRegion(Region r) {
		try {
			File f = new File("plugins/CiteDesSables/regions/" + r.getName() + ".data");
			if (f.delete()) {
				if (r.getHabitation() != null) {
					CiteDesSables.getInstance().getHabitationManager().removeHabitation(CiteDesSables.getInstance().getHabitationManager().getHabitationByName(r.getName()));
					CiteDesSables.getInstance().getHabitationManager().writeToFS();
				}
				System.out.println("Le fichier " + r.getName() + ".data a été supprimé");
			} else {
				System.out.println("Erreur lors de la suppression");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.regions.remove(r);
	}
	
	public HashMap<String, Selection> getSelections() {
		return this.selections;
	}
	
	public Region getRegionByName(String name) {
		Region r = null;
		int i = 0;
		boolean find = false;
		while (i < this.regions.size() && !find) {
			if (name.equalsIgnoreCase(this.regions.get(i).getName())) {
				r = this.regions.get(i);
				find = true;
			}
			i++;
		}
		return r;
	}
	
	public void serializeRegionManager() {
		File dir = new File("plugins/CiteDesSables/regionManager/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/regionManager/regionManager.data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : regionManager.data");
			e.printStackTrace();
		}
	}
	
	public RegionManager readFromFS() {
		RegionManager ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/regionManager/regionManager.data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (RegionManager) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
}
