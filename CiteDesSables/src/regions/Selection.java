package regions;

import java.io.Serializable;

import org.bukkit.Location;

public class Selection implements Serializable {

	private static final long serialVersionUID = -1498227020938018878L;
	private transient Location l1;
	private transient Location l2;
	private double l1X;
	private double l1Y;
	private double l1Z;
	private double l2X;
	private double l2Y;
	private double l2Z;
	private String l1World;
	private String l2World;
	
	public Selection(Location l1, Location l2) {
		this.l1 = l1;
		this.l2 = l2;
		if (this.l1 != null && this.l2 != null) {
			this.l1X = l1.getX();
			this.l1Y = l1.getY();
			this.l1Z = l1.getZ();
			this.l2X = l2.getX();
			this.l2Y = l2.getY();
			this.l2Z = l2.getZ();
			this.l1World = l1.getWorld().getName();
			this.l2World = l2.getWorld().getName();
		}
	}
	
	public Location getFirstLocation() {
		return this.l1;
	}
	
	public Location getSecondLocation() {
		return this.l2;
	}
	
	public void setFirstLocation(Location l) {
		this.l1 = l;
	}
	
	public void setSecondLocation(Location l) {
		this.l2 = l;
	}
	
	public double getFirstLocationX() {
		return this.l1X;
	}
	
	public double getFirstLocationY() {
		return this.l1Y;
	}
	
	public double getFirstLocationZ() {
		return this.l1Z;
	}
	
	public double getSecondLocationX() {
		return this.l2X;
	}
	
	public double getSecondLocationY() {
		return this.l2Y;
	}
	
	public double getSecondLocationZ() {
		return this.l2Z;
	}
	
	public String getFirstLocationWorld() {
		return this.l1World;
	}
	
	public String getSecondLocationWorld() {
		return this.l2World;
	}
}
