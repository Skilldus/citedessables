package regions;

public enum Flag {
	
	PVP("PvP"), BUILD("Build");
	
	private String name;
	
	Flag(String name) {
		this.name = name;
	}
	
	public static Flag getByName(String name) {
		Flag flag = null;
			try {
				flag = Flag.valueOf(name.toUpperCase());
			} catch (Exception e) {	}
		return flag;
	}
	
	public String getName() {
		return this.name;
	}
	
}
