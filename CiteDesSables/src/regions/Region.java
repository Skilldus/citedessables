package regions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cds.CiteDesSables;
import habitations.Habitation;

public class Region implements Serializable {

	private static final long serialVersionUID = -2320607246984140855L;
	private String name;
	private String owner;
	private String entryMessage;
	private String exitMessage;
	private List<Flag> flags;
	private Habitation habitation;
	private List<Area> areas;
	
	public Region(String name, String owner) {
		this.name = name;
		this.habitation = null;
		this.owner = owner;
		this.entryMessage = CiteDesSables.prefix + "§2Vous rentrez dans la région de "+ this.owner;
		this.exitMessage = CiteDesSables.prefix + "§cVous sortez de la région de "+ this.owner;
		this.flags = new ArrayList<Flag>();
		this.areas = new ArrayList<Area>();
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getOwner() {
		return this.owner;
	}
	
	public boolean hasHabitation() {
		return this.habitation != null;
	}
	
	public String getEntryMessage() {
		return this.entryMessage;
	}
	
	public String getExitMessage() {
		return this.exitMessage;
	}
	
	public void setHabitation(Habitation h) {
		this.habitation = h;
	}
	
	public Habitation getHabitation() {
		return this.habitation;
	}
	
	public List<Area> getAreas() {
		return this.areas;
	}
	
	public void addArea(Area area) {
		this.areas.add(area);
	}
	
	public boolean removeArea(Area area) {
		return this.areas.remove(area);
	}
	
	public Flag getByName(String name) {
		Flag ret = null;
		for (Flag flag : this.flags) {
			if (flag.getName().equalsIgnoreCase(name)) {
				ret = flag;
			}
		}
		return ret;
	}
	
	public boolean hasFlag(Flag flag) {
		return this.flags.contains(flag);
	}
	
	public boolean addFlag(Flag flag) {
		return this.flags.add(flag);
	}
	
	public List<Flag> getFlags() {
		return this.flags;
	}
	
	public Flag removeFlag(Flag flag) {
		if (flag != null) this.flags.remove(flag);
		return flag;
	}
	
	public void writeToFS() {
		File dir = new File("plugins/CiteDesSables/regions/");
		if(!dir.exists()) dir.mkdirs();
		try {
            FileOutputStream fos = new FileOutputStream(new File("plugins/CiteDesSables/regions/" + this.name + ".data"));
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(this);
            oos.close();
		} catch (Exception e) {
			System.err.println("Erreur lors de l'ecriture du fichier : " + this.name + ".data");
			e.printStackTrace();
		}
	}
	
	public Region readFromFS() {
		Region ret = null;
		try {
	    	FileInputStream fis = new FileInputStream("plugins/CiteDesSables/regions/" + this.name + ".data");
	    	ObjectInputStream ois = new ObjectInputStream(fis);
	    	ret = (Region) ois.readObject();
	    	ois.close();
		} catch (Exception e) {
		}
		return ret;
	}
	
}
