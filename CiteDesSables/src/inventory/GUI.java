package inventory;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import gui.MarchandGUI;

public class GUI {

	private Map<Class<? extends CustomInventory>, CustomInventory> registeredMenus = new HashMap<>();
	
	private void addMenu(CustomInventory m) {
		this.registeredMenus.put(m.getClass(), m);
	}

	public void open(Player player, Class<? extends CustomInventory> gClass) {

		if (!this.registeredMenus.containsKey(gClass))
			return;

		CustomInventory menu = this.registeredMenus.get(gClass);
		Inventory inv = Bukkit.createInventory(null, menu.getSlots(), menu.getName());
		inv.setContents(menu.getContents(player).get());
		player.openInventory(inv);

	}

	public void registersGUI() {
		addMenu(new MarchandGUI());
	}
	
	public Map<Class<? extends CustomInventory>, CustomInventory> getRegisteredMenus() {
		return this.registeredMenus;
	}
}
